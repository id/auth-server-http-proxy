package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/client"
)

var (
	port    = flag.Int("port", 4041, "A port to bind to on the specified addresses")
	service = flag.String("service", "xmpp", "Service to use for authentication")
	prefix  = flag.String("prefix", "", "Name to use in logs")
	okMsg   = flag.String("ok-msg", "", "A string to return in case of successful auth")
)

func main() {
	flag.Parse()

	if *prefix != "" {
		p := *prefix + ": "
		log.SetPrefix(p)
	}

	log.Printf("Starting on 127.0.0.1:%d\n", *port)

	http.HandleFunc("/", authHandlerFunc)
	log.Fatal(http.ListenAndServe(
		fmt.Sprintf("127.0.0.1:%d", *port),
		nil))
}

type authPayload struct {
	User string `json:"username"`
	Pass string `json:"password"`
}

func authHandlerFunc(w http.ResponseWriter, r *http.Request) {
	var p authPayload
	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		log.Printf("malformed request: %s", err)
		http.Error(w, "malformed request", http.StatusBadRequest)
		return
	}

	c := client.New(client.DefaultSocketPath)
	resp, err := c.Authenticate(r.Context(), &auth.Request{
		Service:  *service,
		Username: p.User,
		Password: []byte(p.Pass),
	})
	if err != nil {
		log.Printf("auth error: %s", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	switch resp.Status {
	case auth.StatusOK:
		w.WriteHeader(http.StatusOK)
		if *okMsg != "" {
			w.Header().Set("Content-Type", "application/text")
			w.Write([]byte(*okMsg))
		}
		return
	case auth.StatusInsufficientCredentials:
		http.Error(w, "not allowed", http.StatusForbidden)
		return
	case auth.StatusError:
		log.Printf("status (user: %s): %s", p.User, resp.Status)
		http.Error(w, "unauthorized", http.StatusUnauthorized)
		return
	}
}
