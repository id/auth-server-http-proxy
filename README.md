# `auth-server` HTTP proxy

This is a simple service to proxy authentication requests from an HTTP enabled
service to the local `auth-server`. It binds on a port (defaulting to `4041`)
only on `127.0.0.1` and tries to connect to the default unix socket of the
`auth-server`.
The request should be a POST with the following payload

```json
{
    "username": "my.user@autistici.org",
    "password": "guesswhat"
}
```

If successful, 200 with no body is returned, 403 otherwise.
