module git.autistici.org/id/auth-server-http-proxy

go 1.15

require (
	contrib.go.opencensus.io/exporter/zipkin v0.1.2 // indirect
	git.autistici.org/id/auth v0.0.0-20230817085209-0fd54184239d
	github.com/golang-migrate/migrate/v4 v4.14.1 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/tstranex/u2f v1.0.0 // indirect
)
